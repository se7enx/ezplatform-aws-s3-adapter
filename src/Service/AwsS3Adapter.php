<?php

namespace ContextualCode\EzPlatformAwsS3Adapter\Service;

use Aws\S3\S3ClientInterface;
use League\Flysystem\AwsS3V3\AwsS3V3Adapter as Base;
use League\Flysystem\Config;
use League\Flysystem\AwsS3V3\VisibilityConverter;
use League\Flysystem\AwsS3V3\MimeTypeDetector;

class AwsS3Adapter extends Base
{
    protected const CACHE_CONTROL_OPTION = 'CacheControl';
    protected const CACHE_CONTROL_VALUE = 'max-age=604800';

    protected $imagesStoragePrefixes;

    public function __construct(
        S3ClientInterface $client,
        $bucket,
        $prefix = '',
        VisibilityConverter $visibility = null,
        MimeTypeDetector $mimeTypeDetector = null,
        array $options = [],
        bool $streamReads = true,
        array $imagesStoragePrefixes = []
    ) {
        parent::__construct($client, $bucket, $prefix, $visibility, $mimeTypeDetector, $options, $streamReads);

        foreach ($imagesStoragePrefixes as $imagePrefix) {
            $this->imagesStoragePrefixes[] = rtrim($imagePrefix, '/') . '/';
        }
    }

    protected function upload($path, $body, Config $config)
    {
        $originalVisibility = $config->get('visibility');
        $originalCacheControl = isset($this->options[self::CACHE_CONTROL_OPTION]) ? $this->options[self::CACHE_CONTROL_OPTION] : null;

        $isImage = false;
        foreach ($this->imagesStoragePrefixes as $imageStoragePrefix) {
            if (strpos($path, $imageStoragePrefix) === 0) {
                $isImage = true;
                break;
            }
        }

        $actualVisibility = $isImage ? self::VISIBILITY_PUBLIC : self::VISIBILITY_PRIVATE;
        $config->set('visibility', $actualVisibility);

        // Set cache control for images
        if ($isImage) {
            $this->options[self::CACHE_CONTROL_OPTION] = self::CACHE_CONTROL_VALUE;
        }

        $result = parent::upload($path, $body, $config);

        $config->set('visibility', $originalVisibility);

        // Reset cache control to the previous value
        if ($isImage) {
            if ($originalCacheControl === null) {
                unset($this->options[self::CACHE_CONTROL_OPTION]);
            } else {
                $this->options[self::CACHE_CONTROL_OPTION] = $originalCacheControl;
            }
        }

        return $result;
    }
}
