<?php

namespace ContextualCode\EzPlatformAwsS3Adapter\Command\Images;

use ContextualCode\EzPlatformAwsS3Adapter\Command\Base;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use Exception;
use Ibexa\Contracts\Core\Repository\Repository;
use Ibexa\Contracts\Core\Repository\Values\Content\LocationQuery;
use Ibexa\Contracts\Core\Repository\Values\Content\Query\Criterion;
use Ibexa\Contracts\Core\Repository\Values\Content\Search\SearchResult;
use Ibexa\Contracts\Core\Repository\Values\ContentType\ContentType;
use Ibexa\Core\FieldType\Image\Value as ImageValue;
use Ibexa\Contracts\Core\SiteAccess\ConfigResolverInterface;
use Ibexa\Bundle\Core\Imagine\ImageAsset\AliasGenerator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateAliases extends Base
{
    private const IMAGE_FIELD_TYPE = 'ezimage';

    protected static $defaultName = 'ezplatform:images:generate-aliases';
    protected $description = 'Generates image aliases';
    protected $options = [
        'parent-location-ids' => [
            'mode' => InputOption::VALUE_OPTIONAL,
            'description' => 'The list of parent location ids (separated by comma)',
            'default' => 1,
        ],
        'content-types' => [
            'mode' => InputOption::VALUE_OPTIONAL,
            'description' => 'The list of content types for which aliases should be generated (separated by comma)',
            'default' => null,
        ],
        'variations' => [
            'mode' => InputOption::VALUE_OPTIONAL,
            'description' => 'The list of image aliases which should be generated (separated by comma)',
            'default' => null,
        ],
    ];

    /** @var SymfonyStyle */
    protected $io;

    /** @var Connection */
    private $db;

    /** @var Repository */
    private $repository;

    /** @var ConfigResolverInterface */
    private $configResolver;

    /** @var AliasGenerator */
    private $aliasGenerator;

    public function __construct(
        Connection $connection,
        ConfigResolverInterface $configResolver,
        Repository $repository,
        AliasGenerator $aliasGenerator
    ) {
        $this->db = $connection;
        $this->configResolver = $configResolver;
        $this->repository = $repository;
        $this->aliasGenerator = $aliasGenerator;

        parent::__construct(null);
    }

    protected function doRun(InputInterface $input): int
    {
        $locationIds = $this->getLocationIds($input->getOption('parent-location-ids'));
        $types = $this->getContentTypesWithImageFields($input->getOption('content-types'));
        $variations = $this->getPossibleImageVariations($input->getOption('variations'));
        $offset = 0;
        $limit = 100;

        do {
            $searchResults = $this->searchContent($locationIds, $types, $offset, $limit);

            if ($offset === 0) {
                $title = 'Generating image aliases for ' . $searchResults->totalCount . ' content items';
                $this->io->title($title);
                $this->progressStart($searchResults->totalCount);
            }

            foreach ($searchResults->searchHits as $hit) {
                $this->progressAdvance();
                $content = $hit->valueObject;
                foreach (array_keys($content->fields) as $fieldIdentifier) {
                    $value = $content->getFieldValue($fieldIdentifier);
                    if ($value instanceof ImageValue === false) {
                        continue;
                    }

                    if (empty($value->id)) {
                        continue;
                    }

                    foreach ($variations as $variation) {
                        try {
                            $this->aliasGenerator->getVariation(
                                $content->getField($fieldIdentifier),
                                $content->versionInfo,
                                $variation
                            );
                        } catch(Exception $e) {
                            $this->io->error($e->getMessage());
                        }
                    }
                }
            }

            $offset += $limit;
        } while (count($searchResults->searchHits));

        $this->progressFinish();

        return 0;
    }

    protected function getLocationIds(string $locations): array
    {
        $locationIds = array_map(static function($v) {
            return (int) trim($v);
        }, explode(',', $locations));

        $locationIds = array_filter($locationIds, static function($v) {
            return $v > 0;
        });

        return array_unique($locationIds);
    }

    protected function getContentTypesWithImageFields(?string $option): array
    {
        $q = $this->db->createQueryBuilder();
        $q->select('t.id, t.identifier')
            ->from('ezcontentclass_attribute', 'f')
            ->leftJoin('f', 'ezcontentclass', 't', 't.id = f.contentclass_id')
            ->where(
                $q->expr()->eq('f.data_type_string', ':image_field_type')
            )->andWhere(
                $q->expr()->eq('f.version', ':status_defined')
            )->andWhere(
                $q->expr()->eq('t.version', ':status_defined')
            )
            ->setParameter('image_field_type', self::IMAGE_FIELD_TYPE, ParameterType::STRING)
            ->setParameter('status_defined', ContentType::STATUS_DEFINED, ParameterType::INTEGER);

        $types = $this->getOptionValues($option);
        if (count($types)) {
            $q->andWhere(
                $q->expr()->in('t.identifier', ':filter_types')
            )->setParameter(':filter_types', $types, Connection::PARAM_STR_ARRAY);
        }

        return $q->execute()->fetchAll();
    }

    protected function getPossibleImageVariations(?string $option): array
    {
        $variations = array_keys($this->configResolver->getParameter('image_variations'));

        $filterVariations = $this->getOptionValues($option);
        if (count($filterVariations)) {
            $variations = array_intersect($variations, $filterVariations);
        }

        return $variations;
    }

    protected function getOptionValues(?string $option): array
    {
        $values = array_map(static function($v) {
            return trim($v);
        }, explode(',', $option));

        $values = array_filter($values, static function($v) {
            return !empty($v);
        });

        return array_unique($values);
    }

    protected function searchContent(
        array $parentLocationIds,
        array $contentTypes,
        int $offset = 0,
        int $limit = 25
    ): SearchResult {
        $subtreeCriterions = [];
        foreach ($parentLocationIds as $locationId) {
            try {
                $locationPath = $this->repository->sudo(static function (Repository $repository) use ($locationId) {
                    return $repository->getLocationService()->loadLocation($locationId)->pathString;
                });
            } catch(Exception $e) {
                continue;
            }

            $subtreeCriterions[] = new Criterion\Subtree($locationPath);
        }

        $contentTypeCriterions = [];
        if (count($contentTypes) === 0) {
            $contentTypes = [['id' => -1]];
        }
        foreach ($contentTypes as $contentTypeInfo) {
            $contentTypeCriterions[] = new Criterion\ContentTypeId((int) $contentTypeInfo['id']);
        }

        if (count($subtreeCriterions) === 0) {
            $filter = new Criterion\LogicalOr($contentTypeCriterions);
        } else {
            $filter = new Criterion\LogicalAnd([
                new Criterion\LogicalOr($subtreeCriterions),
                new Criterion\LogicalOr($contentTypeCriterions)
            ]);
        }
        $query = new LocationQuery(['filter' => $filter]);
        $query->offset = $offset;
        $query->limit = $limit;

        return $this->repository->sudo(static function (Repository $repository) use ($query) {
            return $repository->getSearchService()->findContent($query);
        });
    }
}
