<?php

namespace ContextualCode\EzPlatformAwsS3Adapter\Imagine;

use Ibexa\Bundle\Core\Imagine\IORepositoryResolver as Base;
use Ibexa\Bundle\Core\Imagine\VariationPathGenerator\WebpFormatVariationPathGenerator as VariationPathGenerator;
use Ibexa\Core\Base\Exceptions\NotFoundException;
use Ibexa\Core\IO\IOConfigProvider;
use Ibexa\Core\IO\IOServiceInterface;
use Ibexa\Core\IO\Values\MissingBinaryFile;
use Ibexa\Contracts\Core\Variation\VariationPurger;
use Liip\ImagineBundle\Exception\Imagine\Cache\Resolver\NotResolvableException;
use Liip\ImagineBundle\Imagine\Filter\FilterConfiguration;
use Symfony\Component\Routing\RequestContext;

class IORepositoryResolver extends Base
{
    /** @var IOConfigProvider */
    protected $configResolver;

    /** @var string */
    protected $awsS3BaseUrl;

    public function __construct(
        IOServiceInterface $ioService,
        RequestContext $requestContext,
        FilterConfiguration $filterConfiguration,
        VariationPurger $variationPurger,
        VariationPathGenerator $variationPathGenerator,
        IOConfigProvider $configResolver,
        string $awsS3BaseUrl
    ) {
        $this->ioService = $ioService;
        $this->requestContext = $requestContext;
        $this->filterConfiguration = $filterConfiguration;
        $this->variationPurger = $variationPurger;
        $this->variationPathGenerator = $variationPathGenerator;

        parent::__construct(
            $ioService,
            $requestContext,
            $filterConfiguration,
            $variationPurger,
            $variationPathGenerator
        );

        $this->configResolver = $configResolver;
        $this->awsS3BaseUrl = $awsS3BaseUrl;
    }

    protected function getAwsS3Url(string $path): string
    {
        $storageDir = '/' . ltrim($this->configResolver->getLegacyUrlPrefix(), '/');
        if (strpos($path, $storageDir) === 0) {
            $path = mb_substr($path, strlen($storageDir));
        }

        return $this->awsS3BaseUrl . $path;
    }

    public function resolve($path, $filter): string
    {
        try {
            $binaryFile = $this->ioService->loadBinaryFile($path);
            // Treat a MissingBinaryFile as a not loadable file.
            if ($binaryFile instanceof MissingBinaryFile) {
                throw new NotResolvableException("Variation image not found in $path");
            }

            if ($filter !== static::VARIATION_ORIGINAL) {
                $variationPath = $this->getFilePath($path, $filter);
                $variationBinaryFile = $this->ioService->loadBinaryFile($variationPath);
                $path = $variationBinaryFile->uri;
            } else {
                $path = $binaryFile->uri;
            }

            return strpos($path, '/') === 0 ? $this->getAwsS3Url($path) : $path;
        } catch (NotFoundException $e) {
            throw new NotResolvableException("Variation image not found in $path", 0, $e);
        }
    }
}
