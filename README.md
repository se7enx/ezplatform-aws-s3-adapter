# AWS S3 File Adapter for eZ Platform v3

This bundle provides AWS S3 file adapter, and the key features are:
- Easy to install and configure.
- Each environment uses its own storage within the same AWS S3 Bucket. And the bundle provides the script to sync storage between different environments.
- Direct AWS S3 links are used for generated image aliases.
- It provides an additional script to generate specific image aliases for specific content items. Which might be used as a performance optimization in some cases.
- Binary files are uploaded with private ACL. So it is impossible to access them bypassing Ibexa DXP security rules.

## Installation

1. Using [AWS S3](https://s3.console.aws.amazon.com/s3/home?region=us-east-2) create a new public bucket.

2. Using [AWS IAM](https://console.aws.amazon.com/iam/home?region=us-east-1#/users) create a new "Programmatic access" user with read/write access to the previously created S3 bucket. 

3. Set AWS credentials as environment variables. Run the following command, if the project runs on [Ibexa Cloud](https://www.ibexa.co/products/ibexa-cloud) or [Platform.sh](https://platform.sh/):
    ```bash
    platform variable:create --level=project --name=AWS_ACCESS_KEY_ID \
        --value=<YOUR_KEY_ID> --json=false --sensitive=false --prefix=env \
        --visible-build=false --visible-runtime=true --enabled=true --inheritable=true
    platform variable:create --level=project --name=AWS_SECRET_ACCESS_KEY \
        --value=<YOUR_ACCESS_KEY> --json=false --sensitive=true --prefix=env \
        --visible-build=false --visible-runtime=true --enabled=true --inheritable=true
    ```

4. Add variables to `.env` file:
    ```bash
    ...
    ###> contextualcode/ezplatform-aws-s3-adapter ###
    AWS_FILE_ADAPTER_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
    AWS_FILE_ADAPTER_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
    AWS_FILE_ADAPTER_REGION=us-east-1
    AWS_FILE_ADAPTER_BUCKET=<S3_BUCKET_NAME>
    AWS_FILE_ADAPTER_PREFIX=${PLATFORM_BRANCH}
    ###< contextualcode/ezplatform-aws-s3-adapter ###
    ```

5. Using composer require `contextualcode/ezplatform-aws-s3-adapter`:
    ```bash
    composer require contextualcode/ezplatform-aws-s3-adapter
    ```

6. Update `config/packages/oneup_flysystem.yaml`:
    ```yaml
    oneup_flysystem:
        adapters:
            aws_s3_adapter:
                awss3v3:
                    client: 'amazon.client.aws_s3_adapter'
                    bucket: '%env(AWS_FILE_ADAPTER_BUCKET)%'
                    prefix: '%env(AWS_FILE_ADAPTER_PREFIX)%'
    
    ez_io:
        binarydata_handlers:
            aws_s3:
                flysystem:
                    adapter: aws_s3_adapter
        metadata_handlers:
            dfs:
                legacy_dfs_cluster:
                    connection: doctrine.dbal.default_connection
    
    ezplatform:
        system:
            default:
                io:
                    binarydata_handler: aws_s3
                    metadata_handler: dfs
    ```

7. Migrate the existing files if you are enabing AWS S3 File Adapter on the existing project:
    ```bash
    php -d memory_limit=-1 bin/console ezplatform:io:migrate-files --from=default,default --to=dfs,aws_s3 --no-interaction
    ```

8. Deploy the changes:
    ```bash
    git add composer.json composer.lock symfony.lock .env config/bundles.php config/packages/oneup_flysystem.yaml
    git commit -m "Installed AWS S3 file adapter"
    git push
    ```

## Syncing storage between different environments

This bundle provides a script to sync storage between different environments. This script depends on [AWS CLI](https://aws.amazon.com/cli/). So please make sure it is installed and set up. Usage:

```bash
bin/sync-environment-storage <AWS_S3_BUCKET_NAME> <DESTINATION_ENVIRONMENT> <SOURCE_ENVIRONMENT>
```

So if you want to sync the storage from the `master` environment into the `staging` environment for the project which uses `my-project-storage` AWS S3 bucket you should run:

```bash
bin/sync-environment-storage my-project-storage staging master
```

## Generating image aliases

In addition, this bundle provides `bin/regenerate-image-aliases` script. Which allows regenerating image aliases for specific content. Usage:

```bash
bin/regenerate-image-aliases <CLEAR_BEFORE> <LOCATION_IDS> <CONTENT_TYPES> <ALIASES>
```

To regenerate "medium" and "large" aliases created in the last 24 hours for all folders and articles located under root location (location id: 1), please run:

```bash
bin/regenerate-image-aliases '24 hours ago' '1' 'folder,article' 'medium,large'
```

Depending on the project needs, a similar script might be added to a cron job. So when the page will be rendered its image aliases will be already generated, and no CPU resources are used for image aliases generation. 
